#-*- coding: utf-8 -*-
from django.shortcuts import render
# Create your views here.
from django.views.generic import ListView

from mailer.models import Company,Contact


class IndexView(ListView):
    template_name = "mailer/index.html"
    model = Contact
    paginate_by = 100
    ordering='company'